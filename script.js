var data = {
    "40": {
        "age": "40",
        "joint":"130",
        "single": {
            "male": "64"
            , "female": "104"
        }

    }
,    "41": {
        "age": "41",
        "joint":"132",
        "single": {
            "male": "65"
            , "female": "106"
        }
    }
,    "42": {
        "age": "42",
        "joint":"135",
        "single": {
            "male": "66"
            , "female": "108"
        }
    }
,    "43": {
        "age": "43",
        "joint":"137",
        "single": {
            "male": "67"
            , "female": "110"
        }
    }
,    "44": {
        "age": "44",
        "joint":"140",
        "single": {
            "male": "68"
            , "female": "112"
        }
    }
,    "45": {
        "age": "45",
        "joint":"143",
        "single": {
            "male": "69"
            , "female": "114"
        }
    }
,    "46": {
        "age": "46",
        "joint":"145",
        "single": {
            "male": "71"
            , "female": "116"
        }
    }
,    "47": {
        "age": "47",
        "joint":"148",
        "single": {
            "male": "72"
            , "female": "118"
        }
    }
,    "48": {
        "age": "48",
        "joint":"150",
        "single": {
            "male": "73"
            , "female": "120"
        }
    }
,    "49": {
        "age": "49",
        "joint":"153",
        "single": {
            "male": "75"
            , "female": "123"
        }
    }
,    "50": {
        "age": "50",
        "joint":"156",
        "single": {
            "male": "76"
            , "female": "125"
        }
    }
,    "51": {
        "age": "51",
        "joint":"160",
        "single": {
            "male": "78"
            , "female": "128"
        }
    }
,    "52": {
        "age": "52",
        "joint":"164",
        "single": {
            "male": "80"
            , "female": "131"
        }
    }
,    "53": {
        "age": "53",
        "joint":"168",
        "single": {
            "male": "81"
            , "female": "134"
        }
    }
,    "54": {
        "age": "54",
        "joint":"172",
        "single": {
            "male": "83"
            , "female": "138"
        }
    }
,    "55": {
        "age": "55",
        "joint":"177",
        "single": {
            "male": "86"
            , "female": "142"
        }
    }
,    "56": {
        "age": "56",
        "joint":"181",
        "single": {
            "male": "88"
            , "female": "145"
        }
    }
,    "57": {
        "age": "57",
        "joint":"186",
        "single": {
            "male": "90"
            , "female": "149"
        }
    }
,    "58": {
        "age": "58",
        "joint":"190",
        "single": {
            "male": "92"
            , "female": "152"
        }
    }
,    "59": {
        "age": "59",
        "joint":"196",
        "single": {
            "male": "95"
            , "female": "157"
        }
    }
,    "60": {
        "age": "60",
        "joint":"200",
        "single": {
            "male": "97"
            , "female": "160"
        }
    }
,    "61": {
        "age": "61",
        "joint":"210",
        "single": {
            "male": "102"
            , "female": "168"
        }
    }
,    "62": {
        "age": "62",
        "joint":"221",
        "single": {
            "male": "107"
            , "female": "176"
        }
    }
,    "63": {
        "age": "63",
        "joint":"233",
        "single": {
            "male": "186"
            , "female": "113"
        }
    }
,    "64": {
        "age": "64",
        "joint":"244",
        "single": {
            "male": "119"
            , "female": "195"
        }
    }
,    "65": {
        "age": "65",
        "joint":"257",
        "single": {
            "male": "126"
            , "female": "205"
        }
    }
,    "66": {
        "age": "66",
        "joint":"270",
        "single": {
            "male": "132"
            , "female": "216"
        }
    }
,    "67": {
        "age": "67",
        "joint":"284",
        "single": {
            "male": "140"
            , "female": "227"
        }
    }
,    "68": {
        "age": "68",
        "joint":"298",
        "single": {
            "male": "147"
            , "female": "239"
        }
    }
,    "69": {
        "age": "69",
        "joint":"313",
        "single": {
            "male": "155"
            , "female": "251"
        }
    }
,    "70": {
        "age": "70",
        "joint":"329",
        "single": {
            "male": "163"
            , "female": "263"
        }
    }
,    "71": {
        "age": "71",
        "joint":"355",
        "single": {
            "male": "179"
            , "female": "284"
        }
    }
,    "72": {
        "age": "72",
        "joint":"383",
        "single": {
            "male": "196"
            , "female": "306"
        }
    }
,    "73": {
        "age": "73",
        "joint":"413",
        "single": {
            "male": "215"
            , "female": "330"
        }
    }
,    "74": {
        "age": "74",
        "joint":"449",
        "single": {
            "male": "238"
            , "female": "359"
        }
    }
,    "75": {
        "age": "75",
        "joint":"484",
        "single": {
            "male": "261"
            , "female": "387"
        }
    }
,    "76": {
        "age": "76",
        "joint":"521",
        "single": {
            "male": "286"
            , "female": "417"
        }
    }
,    "77": {
        "age": "77",
        "joint":"562",
        "single": {
            "male": "314"
            , "female": "450"
        }
    }
,    "78": {
        "age": "78",
        "joint":"606",
        "single": {
            "male": "344"
            , "female": "484"
        }
    }
,    "79": {
        "age": "79",
        "joint":"658",
        "single": {
            "male": "380"
            , "female": "526"
        }
    }
};
//var hAge, hAnnual, hG, hS, wAge, wAnnual, wG;
// ("#continue").on('click', function(){
//hAge =  ("#Mage").val();
//hG =  ('[name="gender"]:checked').val();
//  hS =  ('[name="marital"]:checked').val();
//  getMData();
//});
//
//function getMData(){
//  console.log(hAge, hG, hS)
//  console.log(data[hAge][hG][hS]);
//}
//
// Create USD currency formatter.
//var formatter = new Intl.NumberFormat('en-US', {
//  style: 'currency',
//  currency: 'USD',
//});
//$("#annualIncome").blur(function(){
//  var am = $(this).val();
//    var bb = formatter.format(am);
//$(this).val(bb);
//    var b1 = bb.split("$");
//    var b2 = b1[1].split(",")
//    var b3 = b2.join("");
//    var b4 = parseInt(b3)
//});
// Use it.
var annualIncome, sAnnualIncome;
var ageVal = $("#Mage");
var sAgeVal = $("#Sage");
var Continue = $("#continue");
var maritalVal = $('[name="marital"]');
var maritalCheckedVal = $('[name="marital"]:checked');
var genderVal = $('[name="gender"]');
var genderCheckedVal = $('[name="gender"]:checked');
var sGenderVal = $('[name="sGender"]');
var sGenderCheckedVal = $('[name="sGender"]:checked');
var annualIncomeVal = $("#annualIncome");
var sAnnualIncomeVal = $("#sincome");
var seeAnalysis = $("#seeAnalysis");
Continue.on('click', function () {
    var requiredCheck = iRequiredCheckFunction();
    if (requiredCheck === true) {
        if ($('[name="marital"]:checked').val() === "single") {
            var a = ageVal.val();
            var c = $('[name="gender"]:checked').val();
            var b = $('[name="marital"]:checked').val();
            var b1 = annualIncomeVal.val();
            var b2 = b1.split(",")
            var b3 = b2.join("");
            var b4 = parseInt(b3)
            var d = b4 * (0.58 / 100);
            var ae = b4;
            singleAnnual(data[a][b][c], a, c, b, ae);
            $("#spouse").hide();
            stateLtci(d, ae);
            $('#smartwizard').smartWizard("goToStep", 2);
            $('#smartwizard').smartWizard("stepState", [1], "hide");
        }
        else {
            $("#spouse").show();
            $('#smartwizard').smartWizard("stepState", [1], "show");
            $('#smartwizard').smartWizard("goToStep", 1);
            Continue.hide();
            $("#spouseDetails").addClass('spouse-show');
        }
    }
});

function iRequiredCheckFunction() {
    var mCheck = $('[name="marital"]:checked').length;
    var gCheck = $('[name="gender"]:checked').length;
    var aiCheck = parseInt(annualIncomeVal.val());
    if (parseInt(aiCheck) === 0) {
        if ($("#yAnnual").find(".error-text").length === 0) {
            $("#yAnnual").append('<span class="error-text">Annual income must be greater than 0</span>');
        }
    }
    else {
        $("#yAnnual").find(".error-text").remove();
    }
    if (gCheck === 0) {
        if ($("#gen").find(".error-text").length === 0) {
            $("#gen").append('<span class="error-text">This field is Required.</span>');
        }
    }
    else {
        $("#gen").find(".error-text").remove();
    }
    if (mCheck === 0) {
        if ($("#mar").find(".error-text").length === 0) {
            $("#mar").append('<span class="error-text">This field is Required.</span>');
        }
    }
    else {
        $("#mar").find(".error-text").remove();
    }
    if (parseInt(aiCheck) === 0) {
        return false;
    }
    else if (gCheck === 0) {
        return false;
    }
    else if (mCheck === 0) {
        return false;
    }
    else {
        return true;
    }
}
//Analysis
seeAnalysis.on('click', function () {
    var sRequiredCheck = spouseCheckFunction();
    if (sRequiredCheck === true) {
        marriedResult();
        $('#smartwizard').smartWizard("goToStep", 2);
    }
})

function spouseCheckFunction() {
    var genCheck = $('[name="sGender"]:checked').length;
    var incomeCheck = $("#sincome").val();
    var sAgeCheck = sAgeVal.val();
    if (genCheck === 0) {
        if ($("#smar").find(".error-text").length === 0) {
            $("#smar").append('<span class="error-text">This field is Required.</span>');
        }
        return false;
    }
    else {
        $("#smar").find(".error-text").remove();
        return true;
    }
}

function marriedResult() {
    var annualIncome, sAnnualIncome;
    var ageVal = $("#Mage");
    var sAgeVal = $("#Sage");
    var Continue = $("#continue");
    var maritalVal = $('[name="marital"]');
    var maritalCheckedVal = $('[name="marital"]:checked');
    var genderVal = $('[name="gender"]');
    var genderCheckedVal = $('[name="gender"]:checked');
    var sGenderVal = $('[name="sGender"]');
    var sGenderCheckedVal = $('[name="sGender"]:checked');
    var annualIncomeVal = $("#annualIncome");
    var sAnnualIncomeVal = $("#sincome");
    var seeAnalysis = $("#seeAnalysis");
    var sa = sAgeVal.val();
    var sg = sGenderCheckedVal.val();
    var a = ageVal.val();
    var b = genderCheckedVal.val();
    var b1 = annualIncomeVal.val();
    var b2 = b1.split(",")
    var b3 = b2.join("");
    var b4 = parseInt(b3)
    var c1 = sAnnualIncomeVal.val();
    var c2 = c1.split(",")
    var c3 = c2.join("");
    var c4 = parseInt(c3);
    var e = parseInt(b4) + parseInt(c4);
    if (parseInt(c4) != 0) {
        var v=sa>=a?parseFloat(data[sa]["joint"]):parseFloat(data[a]["joint"]);
        var d = (parseInt(b4) + parseInt(c4)) * (0.58 / 100);
        var ad = (parseInt(b4) + parseInt(c4));
        stateLtci(d, ad);
        marriedAnnual(v.toFixed(2), a, b, sa, sg, e);
    }
    else {
        var sgg;
        if (parseInt(c4) === 0) {
            sgg = genderCheckedVal.val();
        }
        var d = (parseInt(b4) + parseInt(c4)) * (0.58 / 100);
        var ad = (parseInt(b4) + parseInt(c4));
        stateLtci(d, ad);
        var value=sa>=a?parseFloat(data[sa]["joint"]):parseFloat(data[a]["joint"]);
        marriedAnnual(value, a, b, sa, sg, e);
    }
}

function stateLtci(data, ae) {
    var df = Math.round(data);
    var value = formator(df);
    $("#stateLTC").text(value);
    tenYearsCost(ae);
    $("#result").addClass('show-details');
}

function singleAnnual(insurance, age, gender, marital, income) {
    var formatedIncome = formator(income);
    var formatedInsurance = formator(insurance*12);
    $("#privateLTC").text(formatedInsurance);
    $("#textOut").html('LTC Options analysis based on a <span class="ltci-dark-text badge">' + gender + '</span> age <span class="ltci-dark-text badge">' + age + '</span>, income of <span class="ltci-dark-text badge">$' + formatedIncome + '</span>');
}

function marriedAnnual(insurance, age, gender, sage, sgender, income) {
    var formatedIncome = formator(income);
    var formatedInsurance = formator(insurance*12);
    $("#privateLTC").text(formatedInsurance);
    $("#textOut").html('LTC Options analysis based on a<span class="ltci-dark-text badge">' + gender + '</span> age <span class="ltci-dark-text badge">' + age + '</span> and <span class="ltci-dark-text badge">' + sgender + '</span> age <span class="ltci-dark-text badge">' + sage + '</span>, combined income of <span class="ltci-dark-text badge">$' + formatedIncome + '</span>');
}

function tenYearsCost(data) {
    var a = data;
    var output;
    $.each(new Array(10), function (index) {
        aa = parseInt(a) * 0.0058;
        output = Math.round(aa.toFixed(2));
        a = parseInt(a) + (parseInt(a) * 0.05);
    })
    var vb = Math.round(output);
    var value = formator(vb);
    $("#tenYears").text("$" + value);
}
// Martial Checked
maritalVal.on('change', function () {
    var martialData = $(this).val();
    showHideSpouse(martialData);
})

function showHideSpouse(data) {
    if (data === "married") {
        Continue.text("Continue");
        if ($("#spouseDetails").hasClass("spouse-show")) {
            Continue.hide();
        }
        else {}
    }
    else {
        Continue.show();
        Continue.text("See Analysis   ");
        $("#spouseDetails").removeClass("spouse-show");
    }
}

function formator(data) {
    var input = data.toString();
    var splitInput = input.split(".");
    var arrayData = splitInput[0].split("");
    var arrayReverse = arrayData.reverse();
    var output = [];
    $.each(arrayReverse, function (index, key) {
        var c = index + 1
        output.push(key);
        if ((c % 3) == 0) {
            if (!(c == arrayReverse.length)) {
                output.push(",");
            }
        }
    })
    var formattedOutput = output.reverse();
    if (splitInput.length == 2) {
        var formattedJoint = formattedOutput.join("") + "." + splitInput[1];
    }
    else {
        var formattedJoint = formattedOutput.join("") + ".00";
    }
    return formattedJoint;
}
//IFrame
(function () {
    'use strict'
    if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
        var msViewportStyle = document.createElement('style')
        msViewportStyle.appendChild(document.createTextNode('@-ms-viewport{width:auto!important}'))
        document.head.appendChild(msViewportStyle)
    }
}())

function postHeight() {
    var innerheight = $("body").innerHeight() + 15;
    parent.postMessage(innerheight, "*");
    console.log(innerHeight);
}
$(document).ready(function () {
    postHeight();
});
$(window).resize(function () {
    postHeight();
});
$(document).ready(function () {
    $(":input").inputmask();
});
// Initialize the stepContent event
$("#smartwizard").on("showStep", function (e, anchorObject, stepIndex, stepDirection) {
    postHeight();
});
//TODO: This is to append the URL with Pharams
//       function onClick() {
//           var age = $("#Mage").val(),
//               income = $("#annualIncome").val(),
//               marital =   $('[name="marital"]:checked').val(),
//               url = "https://www.washingtonltctrust.org/review-your-options?age="+age+"&income="+income+"&martial="+marital;
//           parent.URLparams(url);
//        }